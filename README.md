# Timer Web Application

This is a simple timer applicatino on a static web site. It works 100% with Javascript and only needs a web server to run on.

## Run

	docker run -it --rm -v "$PWD":/usr/share/nginx/html -p 80:80 nginx:stable-alpine

## Check with ESLint

	docker run -it --rm -v $(pwd):/data cytopia/eslint .
