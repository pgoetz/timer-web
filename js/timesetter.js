class Timesetter {
	constructor(callback) {
		this.callback = callback;
		this.last_set = Date.now();
		this.time = '';
	}

	set(digit, timestamp) {
		if(timestamp - this.last_set > 1000) {
			this.time = '';
		}

		this.last_set = timestamp;
		this.time += digit;

		setTimeout(() => {
			this.callback(this.time);
		}, 1000);
	}
}

export default Timesetter;
