class Time {
	constructor(minute = 2, second = 37) {
		this.minute = minute;
		this.second = second;
	}

	static parse(time) {
		let result = new Time();

		result.validate(time);

		return result;
	}

	validate(time) {
		let match = time.match(/(\d+):(\d+)/);

		if(match && match.length === 3) {
			this.minute = Number.parseInt(match[1]);
			this.second = Number.parseInt(match[2]);
		} else {
			this.minute = 2;
			this.second = 37;
		}
	}

	to_string() {
		return `${this.minute}:${this.second.toString().padStart(2, '0')}`;
	}
}

export default Time;
