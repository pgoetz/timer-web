import Time from './time.js';

class Timer {
	constructor(output, audio, initial_time) {
		this.output = output;
		this.audio = audio;
		this.initial_time = initial_time;
		this.current_time = initial_time;
		this.running = false;

		this.show_time(initial_time);
	}

	show_time(time) {
		this.output.textContent = time.to_string();
	}

	start() {
		this.running = true;
		this.count_down();
	}

	stop() {
		this.running = false;
	}

	count_down() {
		setTimeout(() => {
			this.show_time(this.current_time);

			if(this.current_time.second == 0) {
				this.current_time = new Time(this.current_time.minute - 1, 59);
			} else {
				this.current_time = new Time(this.current_time.minute, this.current_time.second - 1);
			}

			if(this.current_time.minute === 0 && this.current_time.second === 0) {
				this.done();
			} else {
				if(this.running) {
					this.count_down();
				}
			}
		}, 1000);
	}

	done() {
		this.show_time(new Time(0, 0));
		this.audio.play();
		this.current_time = this.initial_time;
		this.running = false;
	}
}

export default Timer;
